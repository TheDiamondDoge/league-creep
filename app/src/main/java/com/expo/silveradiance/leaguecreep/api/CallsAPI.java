package com.expo.silveradiance.leaguecreep.api;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;

import com.expo.silveradiance.leaguecreep.MatchView;
import com.expo.silveradiance.leaguecreep.R;
import com.expo.silveradiance.leaguecreep.models.Summoner;

import org.w3c.dom.Text;

import java.util.Map;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.HttpException;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by itryt on 17.12.2016.
 */

public class CallsAPI {
    private Activity mActivity = null;
    private static final int MY_PERMISSIONS_REQUEST_INTERNET = 352;
    private Retrofit mRetrofit = null;
    public CompositeSubscription mCompositeSubscribtion = new CompositeSubscription();
    public CallsAPI(Activity a, String l){
        mActivity = a;
        String mBaseLink = l;
        RxJavaCallAdapterFactory rxAdapter = RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io());

        mRetrofit = new Retrofit.Builder()
                .baseUrl(mBaseLink)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(rxAdapter)
                .build();


    }
    public void apiCall(String mSSummName, Subscriber mSubscriber){
        if (ContextCompat.checkSelfPermission(mActivity,
                Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity,
                    Manifest.permission.INTERNET)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(mActivity,
                        new String[]{Manifest.permission.INTERNET},
                        MY_PERMISSIONS_REQUEST_INTERNET);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }


        LeagueAPI leagueAPI = mRetrofit.create(LeagueAPI.class);

        Observable<Map<String,Summoner>> get = leagueAPI.getSummoner(mSSummName);

        Subscription subscription = get
                .subscribeOn(Schedulers.io()) // optional if you do not wish to override the default behavior
                .observeOn(AndroidSchedulers.mainThread()).subscribe(mSubscriber);
        mCompositeSubscribtion.add(subscription);


    }
}
