package com.expo.silveradiance.leaguecreep.api;

import com.expo.silveradiance.leaguecreep.models.Summoner;

import java.util.Map;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

public interface LeagueAPI {
    @GET("lol/euw/v1.4/summoner/by-name/{summname}?api_key=RGAPI-a8a83fac-e5ff-4847-8545-526e463b0b74")
    Observable<Map<String,Summoner>> getSummoner(@Path("summname") String summname);
}