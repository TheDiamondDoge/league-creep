package com.expo.silveradiance.leaguecreep;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.expo.silveradiance.leaguecreep.api.CallsAPI;
import com.expo.silveradiance.leaguecreep.api.LeagueAPI;
import com.expo.silveradiance.leaguecreep.models.Summoner;

import java.util.Iterator;
import java.util.Map;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.HttpException;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class IntroActivity extends AppCompatActivity {
    private EditText mETSummName = null;
    private Button mBSearch = null;
    private TextView mTVHello = null;
    private CallsAPI mCalls = null;
    public static final String BASE_URL = "https://euw.api.pvp.net/api/";
    private Activity mActivity = null;
    private Subscriber mSubscriber = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        mActivity = this;
        mSubscriber = new Subscriber<Map<String,Summoner>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                // cast to retrofit.HttpException to get the response code
                if (e instanceof HttpException) {
                    HttpException response = (HttpException)e;
                    int code = response.code();
                }
            }

            @Override
            public void onNext(Map m) {
                TextView hellow = (TextView) mActivity.findViewById(R.id.intro_hello_line);
                Summoner mSumm = (Summoner) m.values().toArray()[0];
                hellow.setText(mSumm.getId().toString());
                Intent mIntent = new Intent(mActivity, MatchView.class);
                Bundle extras = mIntent.getExtras();
                extras.putInt("summ_id", mSumm.getId());

            }
        };

        mETSummName = (EditText) findViewById(R.id.intro_summ_name);
        mBSearch = (Button) findViewById(R.id.intro_search);
        mTVHello = (TextView) findViewById(R.id.intro_hello_line);


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCalls.mCompositeSubscribtion.unsubscribe();
    }

    public void searchClicked(View v){
        String mSResult =  mETSummName.getText().toString().trim();
        if(mSResult.isEmpty()){
            return;
        }else{
            mTVHello.setText(mSResult);
            mCalls = new CallsAPI(this,BASE_URL);
            mCalls.apiCall(mSResult,mSubscriber);
        }


    }

    public void callShistormo(View v){
        CallsAPI mCalls = new CallsAPI(this,BASE_URL);
        mCalls.apiCall("Shistormo",mSubscriber);
    }





}
